<?php
    class Kamar extends Connect{
        public function insert_kamar($id_tipekamar, $fasilitas_kamar, $fasilitas, $jml_kamar){

            $connect = parent::connection();
            parent::set_name();

            $sql = "INSERT INTO tb_kamar (id_kamar, id_tipekamar, fasilitas_kamar, fasilitas, jml_kamar) VALUES(NULL,?,?,?,?)";

            $sql = $connect->prepare($sql);

            $sql->bindValue(1, $id_tipekamar);
            $sql->bindValue(2, $fasilitas_kamar);
            $sql->bindValue(3, $fasilitas);
            $sql->bindValue(4, $jml_kamar);

            $sql->execute();

            return $result=$sql->fetchAll();
        }

        public function get_data(){

            $connect =  parent::connection();
            parent::set_name();

            // sql join table user, ticket, category
            $sql = "SELECT 
                    tb_kamar.id_kamar,
                    tb_kamar.id_tipekamar, 
                    tb_kamar.fasilitas_kamar, 
                    tb_kamar.fasilitas, 
                    tb_kamar.jml_kamar,
                    tb_kamar.id_tipekamar,
                    tb_tipekamar.tip_name
                    FROM 
                    tb_kamar 
                    INNER JOIN tb_tipekamar ON tb_kamar.id_tipekamar = tb_tipekamar.id_tipekamar";
            
            $sql = $connect->prepare($sql);
            $sql->execute();
            return $result = $sql->fetchAll();


        }
        public function delete_kamar($id_kamar)
        {
            $connect = parent::connection();
            parent::set_name();
            
            $sql = "DELETE FROM tb_kamar WHERE id_kamar=?";
            $sql = $connect->prepare($sql);
            $sql->bindValue(1, $id_kamar);
            $sql->execute();
            return $result = $sql->fetchAll();
        }

       
    }