<?php
    class User extends Connect{

        public function login(){
            $connect=parent::connection();
            parent::set_name();

            if(isset($_POST["submit"])){
                $email = $_POST["user_email"];
                $pass = $_POST["user_pass"];
                $role = $_POST["role_id"];

                if(empty($email) and empty($pass)){
                    header("Location:".connect::base_url()."index.php?m=2");
                    exit();
                }else{
                    $sql = "SELECT * FROM tb_user WHERE user_email=? and user_pass=? and role_id=? and status=1";
                    $stmt = $connect->prepare($sql);
                    $stmt->bindValue(1, $email);
                    $stmt->bindValue(2, $pass);
                    $stmt->bindValue(3, $role);
                    $stmt->execute();
                    $result = $stmt->fetch();

                    if(is_array($result) and count($result)>0){
                        $_SESSION["user_id"]=$result["user_id"];
                        $_SESSION["user_fullname"]=$result["user_fullname"];
                        $_SESSION["user_nickname"]=$result["user_nickname"];
                        $_SESSION["role_id"]=$result["role_id"];
                        header("Location:".connect::base_url()."view/Home/");
                        exit();
                    }else{
                        header("Location:".connect::base_url()."index.php?m=1");
                        exit();
                    }
                }
            }
        }

        public function insert_user($role_id, $user_fullname, $user_nickname, $user_email, $user_pass){
            $connect=parent::connection();
            parent::set_name();

            $sql = "INSERT INTO tb_user (user_id, role_id, user_fullname, user_nickname, user_email, user_pass, created_at, updated_at, deleted_at, status) VALUES (NULL, ?,?,?,?,?now(),NULL,NULL,1 )";

            $sql = $connect->prepare($sql);
            $sql->bindValue(1, $role_id);
            $sql->bindValue(2, $user_fullname);
            $sql->bindValue(3, $user_nickname);
            $sql->bindValue(4, $user_email);
            $sql->bindValue(5, $user_pass);
            $sql->execute();
            return $result = $sql->fetchAll();
        }

        public function update_user($user_id,$role_id, $user_fullname, $user_nickname, $user_email, $user_pass){
            $connect=parent::connection();
            parent::set_name();
            $sql = "UPDATE tb_user
                    SET
                    role_id = ?, user_fullname = ?, user_nickname = ?, user_email = ?, user_pass = ?
                    WHERE
                    user-id = ?";
            $sql = $connect->prepare($sql);
            $sql->bindvalue(1, $user_id);
            $sql->bindValue(2, $role_id);
            $sql->bindValue(3, $user_fullname);
            $sql->bindValue(4, $user_nickname);
            $sql->bindValue(5, $user_email);
            $sql->bindValue(6, $user_pass);
            $sql->execute();
            return $result = $sql->fetchAll();
        }

        public function get_all_user(){
            $connect=parent::connection();
            parent::set_name();
            $sql = "SELECT * FROM tb_user WHERE status='1'";
            $sql = $connect->prepare($sql);
            $sql->execute();
            return $result = $sql->fetchAll();
        }

        public function get_user($user_id){
            $connect=parent::connection();
            parent::set_name();
            $sql = "SELECT FROM tb_user WHERE user_id=?";
            $sql = $connect->prepare($sql);
            $sql->bindvalue(1, $user_id);
            $sql->execute();
            return $result = $sql->fetchAll();
        }

        public function delete_user($user_id){
            $connect=parent::connection();
            parent::set_name();
            $sql = "DELETE FROM tb_user WHERE user-id = ?";
            $sql = $connect->prepare($sql);
            $sql->bindvalue(1, $user_id);
            $sql->execute();
            return $result = $sql->fetchAll();
        }
    }