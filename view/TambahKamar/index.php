<?php
    require_once("../../config/Connect.php");
    if(isset($_SESSION["user_id"]) && $_SESSION["role_id"]==1){

?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Hotel Hebat Application|Tambah Kamar</title>

    <!-- Link -->
	<?php require_once("../LayoutPartial/link.php"); ?>
    <!-- end link -->

</head>
<body class="with-side-menu">

    <!-- header -->
	<?php require_once("../LayoutPartial/header.php"); ?>
    <!-- end header -->

	<div class="mobile-menu-left-overlay"></div>

    <!-- nav -->
	<?php require_once("../LayoutPartial/nav.php"); ?>
    <!-- end nav -->

	<div class="page-content">
		<div class="container-fluid">
        <header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Form Tambah Data Kamar</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Home</a></li>
								<li class="active">Data Kamar Baru</li>
							</ol>
						</div>
					</div>
				</div>
		</header>
            <div class="box-typical box-typical-padding">
        <h5 class="m-t-lg with-border">Tambah Data Kamar</h5>
		<form action="POST" id="ticket-form">
                <div class="row">

					<!-- user ID -->
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $_SESSION["user_id"]?>">

					<div class="col-lg-6">
                        <fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Tipe Kamar</label>
                            <select id="cat_select" name="id_tipekamar" class="form-control">
								
							</select>
						</fieldset>
					</div>
					<div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Fasilitas Kamar</label>
							<input type="text" class="form-control" id="fasilitas_kamar" placeholder="Fasilitas Kamar" name="fasilitas_kamar">
						</fieldset>
					</div>
                </div><!--.row-->

                <div class="row">
                    <div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Fasilitas Umum</label>
							<input type="text" class="form-control" id="fasilitas" placeholder="Fasilitas Umum" name="fasilitas">
						</fieldset>
					</div>

                    <div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Jumlah Kamar</label>
							<input type="text" class="form-control" id="jml_kamar" placeholder="jumlah Kamar" name="jml_kamar">
						</fieldset>
					</div>
                    <div class="col-xs-12 m-t-md">
                        <button type="submit" name="action-save" value="save" class="btn btn-rounded btn-inline btn-success-outline">Simpan</button>
                        <button type="reset" class="btn btn-rounded btn-inline btn-primary-outline">Batal</button>
                    </div>
                </div>
                
		</form> <!-- FORM TICKET BARU -->
		
		</div><!--.container-fluid-->
	</div><!--.page-content-->

    <!-- script -->
	<?php require_once("../LayoutPartial/script.php"); ?>
    <!-- end script -->
    <script src="tambahKamar.js" type="text/javascript"></script>

</body>
</html>
<?php
    }else{
        header("Location: ".Connect::base_url()."index.php");
    }
?>