function init(){
	$("#ticket-form").on("submit", function(e){
		saveAndEdit(e);
	})
}

		$(document).ready(function() {
			$.post("../../controller/Category.php?op=combo", function(data, status){
				$('#cat_select').html(data); 
			})

		
		});

		function saveAndEdit(e){
			e.preventDefault();
			var formData = new FormData($("#ticket-form")[0]);

			// Validate the form  before insert

			if ($('#fasilitas_kamar').val() == '' || $('#fasilitas').val() == ''){
				swal("Perhatian", "Silahkan Isi Form Terlebih Dahulu", "warning");
			} else {
			$.ajax({
				url: "../../controller/TambahKamar.php?op=insert",
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {
					$('#nama_pemesanan').val('');
                    $('#fasilitas_kamar').val('');
                    $('#fasilitas').val('');
					$('#jml_kamar').val('');
					swal("Selamat", "Tiket Baru Disimpan", "success");
				}
			})
		}
		}

		init();