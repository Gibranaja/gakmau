<?php
    require_once("../../config/Connect.php");
    if(isset($_SESSION["user_id"])){

?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Hotel Hebat Application|Data Kamar</title>

    <!-- Link -->
	<?php require_once("../LayoutPartial/link.php"); ?>
    <!-- end link -->

</head>
<body class="with-side-menu">

    <!-- header -->
	<?php require_once("../LayoutPartial/header.php"); ?>
    <!-- end header -->

	<div class="mobile-menu-left-overlay"></div>

    <!-- nav -->
	<?php require_once("../LayoutPartial/nav.php"); ?>
    <!-- end nav -->

	<div class="page-content">
		<div class="container-fluid">
        <header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Form Data Kamar</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Home</a></li>
								<li class="active">Data Kamar</li>
							</ol>
						</div>
					</div>
				</div>
		</header>

        <section class="card">
				<div class="card-block">
					<table id="ticket-table" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th style="width: 5%;">No.</th>
							<th style="width: 20%;">Tipe Kamar</th>
							<th style="width: 20%;">Fasilitas Kamar</th>
							<th style="width: 20%;">Fasilitas Umum</th>
							<th style="width: 15%;">Jumlah Kamar</th>
							<th>Aksi</th>
						</tr>
						</thead>
        
						<tbody>
						<tr>
							<td>Tiger Nixon</td>
							<td>System Architect</td>
							<td>Edinburgh</td>
							<td>61</td>
						</tr>
						<tr>
							<td>Garrett Winters</td>
							<td>Accountant</td>
							<td>Tokyo</td>
							<td>63</td>
						</tr>
						
						</tbody>
					</table>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

    <!-- script -->
	<?php require_once("../LayoutPartial/script.php"); ?>
    <!-- end script -->
    <script src="dataKamar.js" type="text/javascript"></script>

</body>
</html>
<?php
    }else{
        header("Location: ".Connect::base_url()."index.php");
    }
?>