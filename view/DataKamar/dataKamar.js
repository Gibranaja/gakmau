var table;
var user_id = $('#user_id').val();
var role_id = $('#role_id').val();

function init(){

}

$(document).ready(function(){

    if(role_id == 1){
        var t = table = $('#ticket-table').dataTable({
            "aProcessing" : true, 
            "aServerSide" : true,
            "ajax": {
                url: "../../controller/TambahKamar.php?op=getData", 
                type: "POST",
                dataType: "json",
                data: { user_id: user_id },
                error: function (e) {
                    console.log(e.responseText);
                }
            },
            
            
        }).DataTable();
    }else{
        var t = table = $('#ticket-table').dataTable({
            "aProcessing" : true, 
            "aServerSide" : true,
            "ajax": {
                url: "../../controller/TambahKamar.php?op=get_data_petugas",
                type: "POST",
                dataType: "json",
                error: function (e) {
                    console.log(e.responseText);
                }
            },
           
            
        }).DataTable();
    }
    

    

    t.on( 'order.dt search.dt', function () {
        let i = 1;
 
        t.cells(null, 0, {search:'applied', order:'appli                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ed'}).every( function (cell) {
            this.data(i++);
        } );
    } ).draw();
    
    
    
    

    
}); 


function get(id_kamar){
    console.log(id_kamar);
}


function delete_kamar(id_kamar) {
    // console.log(id_kamar);
    
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          })
        // Ajax config
        $.ajax({
            
            type: "POST", //we are using GET method to get data from server side
            url: '../../controller/TambahKamar.php?op=delete_kamar', // get the route value
            data: {id_kamar:id_kamar}, //set data
            beforeSend: function () {//We add this before send to disable the button once we submit it so that we prevent the multiple click
                
            },
            success: function (response) {//once the request successfully process to the server side it will return result here
                $('#ticket-table').DataTable().ajax.reload();
                then((result) => {
                    if (result.isConfirmed) {
                      Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                      )
                    }
                  })
                // Reload lists of employees
                all();

                alert(response)
            }
        });
    }

init();