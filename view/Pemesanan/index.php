<?php
    require_once("../../config/Connect.php");
    

?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Hotel Hebat Application|Pemesanan</title>

    <!-- Link -->
	<?php require_once("../LayoutPartial/link.php"); ?>
    <!-- end link -->

</head>
<body class="with-center-menu">

	<div class="page-content">
		<div class="container-fluid">
        <header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>Form Pemesanan</h3>
							<ol class="breadcrumb breadcrumb-simple">
								<li><a href="#">Home</a></li>
								<li class="active">Pemesanan</li>
							</ol>
						</div>
					</div>
				</div>
		</header>
            <div class="box-typical box-typical-padding">
        <h5 class="m-t-lg with-border">Tambah Pemesanan Baru</h5>
		<form action="POST" id="ticket-form">
                <div class="row">

					<div class="col-lg-6">
                        <fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Tipe Kamar</label>
                            <select id="cat_select" name="id_tipekamar" class="form-control">
								
							</select>
						</fieldset>
					</div>
					<div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Nama Pemesan</label>
							<input type="text" class="form-control" id="nama_pemesanan" placeholder="Nama Pemesan" name="nama_pemesanan">
						</fieldset>
					</div>
				</div><!--.row-->

				<div class="row">
                    <div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Email</label>
							<input type="text" class="form-control" id="email" placeholder="Email" name="email">
						</fieldset>
					</div>
					
                    <div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">No Handphone</label>
							<input type="text" class="form-control" id="no_hp" placeholder="No Handphone" name="no_hp">
						</fieldset>
					</div>
				</div>


				<div class="row">
                   
                    <div class="col-lg-6">
						<div class="form-group">
							<div class='input-group date datetimepicker-1'>
                            <label class="form-label" for="exampleInputPassword1">Check In</label>
								<input type='date' class="form-control" id="check_in" name="check_in">
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Nama Tamu</label>
							<input type="text" class="form-control" id="nama_tamu" placeholder="Nama Tamu" name="nama_tamu">
						</fieldset>
					</div>
				</div>

				<div class="row">
                    <div class="col-lg-6">
						<div class="form-group">
							<div class='input-group date datetimepicker-1'>
                            <label class="form-label" for="exampleInputPassword1">Check Out</label>
								<input type='date' class="form-control" id="check_out" name="check_out">
							</div>
						</div>
					</div>
                    <div class="col-lg-4">
						<fieldset class="form-group">
							<label class="form-label" for="exampleInputPassword1">Jumlah</label>
							<input type="text" class="form-control" id="jml_kamar" placeholder="Jumlah" name="jml_kamar">
						</fieldset>
					</div>
				</div>

                <div class="row">   
                    <div class="col-xs-12 m-t-md">
                        <button type="submit" name="action-save" value="save" class="btn btn-rounded btn-inline btn-success-outline">Simpan</button>
                        <button type="reset" class="btn btn-rounded btn-inline btn-primary-outline">Batal</button>
                    </div>
				</div> 
				
		</form> <!-- FORM TICKET BARU -->
		
		</div><!--.container-fluid-->
	</div><!--.page-content-->

    <!-- script -->
	<?php require_once("../LayoutPartial/script.php"); ?>
    <!-- end script -->
    <script src="pemesanan.js" type="text/javascript"></script>

</body>
</html>
