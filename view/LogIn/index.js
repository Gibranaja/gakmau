function init(){

}

$(document).ready(function (){

});

$(document).on("click", "#btnAkses", function () {
    if ($('#role_id').val() == 1) {
        $('#lblHeader').html("Akses Resepsionis");
        $('#btnAkses').html("Akses Admin");
        $('#role_id').val(2);
        $('#img-icon').attr('src', 'public/2.png');
    } else {
        $('#lblHeader').html("Akses Admin");
        $('#btnAkses').html("Akses Resepsionis");
        $('#role_id').val(1);
        $('#img-icon').attr('src', 'public/1.png');
    }
});

init();