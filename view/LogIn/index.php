<?php
    require_once("../../config/Connect.php");

    if(isset($_POST["submit"]) and $_POST["submit"]=="Yes"){
        require_once("../../models/User.php");
        $user = new User();
        $user->login();
    }
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Hotel Hebat Application</title>

	<link href="../../public/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="../../public/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="../../public/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="../../public/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="../../public/img/favicon.png" rel="icon" type="image/png">
	<link href="../../public/img/favicon.ico" rel="shortcut icon">

    <link rel="stylesheet" href="../../public/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="../../public/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../../public/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/main.css">
</head>
<body>

    <div class="page-center">
        <div class="page-center-in">
            <div class="container-fluid">
                <form class="sign-box" action="" method="POST" id="login_form">
                    <div class="sign-avatar">
                        <img src="../../public/1.png" alt="" id="img-icon">
                    </div>
                    <input type="hidden" id="role_id" name="role_id" value="1">
                    <header class="sign-title" id="lblHeader">Akses Admin</header>
                    <!-- CHECK CONDITION FOR ALERT -->

                    <?php
                        if(isset($_GET["m"])){
                            switch($_GET["m"]){
                                case "1";
                            
                    ?>
                        <div class="alert alert-danger alert-icon alert-close alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
							<i class="font-icon font-icon-warning"></i>
							email atau password salah
						</div>
                    <?php
                        break;
                        case "2";
                    ?>
                        <div class="alert alert-warning alert-icon alert-close alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
							<i class="font-icon font-icon-warning"></i>
							Silahkan masukkan email dan password anda!
						</div>
                    <?php
                            break;
                            }
                        }
                    ?>
                        <!-- END CHECK CONDITIONFOR ALERT -->

                    <div class="form-group">
                        <input type="text" id="user_email" name="user_email" class="form-control" placeholder="E-Mail Address"/>
                    </div>
                    <div class="form-group">
                        <input type="password" id="user_pass" name="user_pass" class="form-control" placeholder="Password"/>
                    </div>
                    <div class="form-group">
                        
                        <div class="float-right reset">
                            <a href="reset-password.html">Lupa Password</a>
                        </div>
                        <div class="float-left reset">
                            <a href="#" id="btnAkses">Akses Resepsionis</a>
                        </div>
                    </div>
                    <input type="hidden" name="submit" value="Yes">
                    <button type="submit" class="btn btn-rounded">Sign in</button>
                   
                </form>
            </div>
        </div>
    </div><!--.page-center-->


<script src="../../public/js/lib/jquery/jquery.min.js"></script>
<script src="../../public/js/lib/tether/tether.min.js"></script>
<script src="../../public/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="../../public/js/plugins.js"></script>
    <script type="text/javascript" src="../../public/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>
<script src="../../public/js/app.js"></script>
<script src="index.js" type="text/javascript"></script>
</body>
</html>