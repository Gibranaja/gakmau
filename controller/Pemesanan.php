<?php
    require_once("../config/Connect.php");
    require_once("../models/Pemesanan.php");

    $pemesanan = new Pemesanan();

    switch($_GET["op"]){
        case "insert":
            $pemesanan->insert_pemesanan($_POST["nama_pemesanan"],
            $_POST["email"],
            $_POST["no_hp"],
            $_POST["nama_tamu"],
            $_POST["id_tipekamar"],
            $_POST["check_in"],
            $_POST["check_out"],
            $_POST["jml_kamar"]);
        break;
        case "getData" :
            $data_hasil = $pemesanan->get_data();
            $data = Array();
            foreach($data_hasil as $row){
                $sub_array = array();
                $sub_array[] = $row["id_pemesanan"];
                $sub_array[] = $row["nama_pemesanan"];
                $sub_array[] = $row["email"];
                $sub_array[] = $row["no_hp"];
                $sub_array[] = $row["nama_tamu"];
                $sub_array[] = $row["tip_name"];   
                if($row["status_kam"] == "on site"){
                    $sub_array[] = '<span class="label label-success">on site</span>';
                }else{
                    $sub_array[] = '<span class="label label-danger">out side</span>';
                }
                $sub_array[] = date("d-M-Y", strtotime($row["check_in"]));
                $sub_array[] = date("d-M-Y", strtotime($row["check_out"]));
                $sub_array[] = $row["jml_kamar"];
                $data[] = $sub_array;
            }
            $result = array(
                "sEcho" => 1,
                "iTotalRecords" =>count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );

            echo json_encode($result);
        break;
    }
?>