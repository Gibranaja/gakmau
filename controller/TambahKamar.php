<?php
    require_once("../config/Connect.php");
    require_once("../models/TambahKamar.php");

    $pemesanan = new Kamar();

    switch($_GET["op"]){
        case "insert":
            $pemesanan->insert_kamar($_POST["id_tipekamar"],
            $_POST["fasilitas_kamar"],
            $_POST["fasilitas"],
            $_POST["jml_kamar"]);
        break;
        case "getData" :
            $data_hasil = $pemesanan->get_data();
            $data = Array();
            foreach($data_hasil as $row){
                $sub_array = array();
                $sub_array[] = $row["id_kamar"];
                $sub_array[] = $row["tip_name"];
                $sub_array[] = $row["fasilitas_kamar"];
                $sub_array[] = $row["fasilitas"];
                $sub_array[] = $row["jml_kamar"];
                
                $sub_array[] = '<button type="button" onClick="edit('.$row["id_kamar"].');" id="'.$row["id_kamar"].'" class="btn btn-info btn-sm" title="Edit Reservasi">
                    <div>
                        <i class="fa fa-edit"></i>
                    </div>
                </button> |
                <button type="button" onClick="delete_kamar('.$row["id_kamar"].');" id="'.$row["id_kamar"].'"
                class="btn btn-danger btn-sm" title="Delete Data">
                    <div>
                        <i class="fa fa-trash"></i>
                    </div>
                </button>';
                

                $data[] = $sub_array;
            }
            $result = array(
                "sEcho" => 1,
                "iTotalRecords" =>count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );

            echo json_encode($result);
        break;
        case "delete_kamar":
            $pemesanan->delete_kamar($_POST["id_kamar"]);
        break;
    }
?>